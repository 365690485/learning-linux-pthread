CC=gcc
CFLAGS=-I/usr/local
LDFLAGS=-lpthread -L/usr/lib
OPTS=-Wall -O2
fork:fork.c
	gcc -o fork fork.c $(CFLAGS) $(LDFLAGS) $(OPTS)

zval:zval.c core.h
	gcc -o zval zval.c $(CFLAGS) $(OPTS)

hashtable:hashtable.c core.h
	gcc -o hashtable hashtable.c $(CFLAGS) $(OPTS)