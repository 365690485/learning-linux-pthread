/**
 * 实现PHP Zend内核中的通用变量类型zval
 *
 */
#include "core.h"

int main(int argc, char const *argv[])
{
	zval *str_val, *l_val;

	MAKE_STD_ZVAL(str_val);

	ZVAL_STRINGL(str_val, "zval test\n", strlen("zval test\n"), 1);

	printf("str_val:%s\n", Z_STRVAL_P(str_val));

	MAKE_STD_ZVAL(l_val);

	ZVAL_LONG(l_val, 10000);

	printf("l_val:%ld\n", Z_LVAL_P(l_val));

	return 0;
}